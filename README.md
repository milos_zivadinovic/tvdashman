# TvDashMan
Tv Dashboard Manager for random data. Currently it is just a scaffolded React project that is slowly being built up.

For starters, going to have a basic weather prompt for my current location.

## Technical details

I want to brush up on my React skills. Besides looking at video tutorials, I believe that doing real stuff, even if it is bad, is a great starting point.

I also wanted to make a project that uses DDD as much as possible. So far it is going well, but there is a lot of formal stuff that I need to learn.

### Technologies currently used:
* JavaScript
* React

### Technologies that will probably get used:
* TypeScript - I really like it. Did it before, and I just love working with it
* Redux - Also worked with it before, but I believe that I have still to learn with it
* Immutable.js (or alternative) - As a fan of functional programming, I tend to apply its principles as much as I can
